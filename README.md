# ingress-nginx

For handling Kerberos through the ingress nginx controller, nginx can be compiled to support GSSAPI. Thus custom container images are unavoidable to be used for the nginx ingress controller. 
Patching and custom build of the kubernetes ingress-nginx controller available at: https://github.com/kubernetes/ingress-nginx

# Upgrade Procedure

Until we introduce Kerberos to upstream or a form of automation, we must take the following steps when a new version of nginx is released.

1. Check for [latest releases](https://github.com/kubernetes/ingress-nginx/releases).
2. wget and unzip the latest/needed release source code locally.
3. Locate the ingress-nginx-nginx-0.25.0/images/nginx/rootfs/build.sh script and make a copy:
```
~$ cp build.sh build.sh.orig
```
4. Edit the build.sh script to include the following:

At the end of the export section in the beginning we need to ensure the following environment is set:
```
export AUTH_VERSION=1.1.0
export DEBIAN_FRONTEND="noninteractive"
```

Next, during the installation of dependent packages we need to add Kerberos libraries:
```
apt-get update && apt-get dist-upgrade -y
...
  krb5-user libkrb5-dev \
```

Within additional module section, instruct the build to download nginx version as follows:

```
get_src 7b4a0a4eada289096ce5b30b473b50aca3a59735fb1615c2ecdea4ec7f2fcaf5 \
        "https://github.com/stnoonan/spnego-http-auth-nginx-module/archive/v$AUTH_VERSION.tar.gz"
```
Finally, explicitely add module to the list to include within the configuration:

```
WITH_MODULES = " ...

  --add-module=$BUILD_PATH/spnego-http-auth-nginx-module-${AUTH_VERSION}"
```

5. Having made the changes, we can now generate the patch as follows:

```
~$ diff -u build.sh.orig build.sh > nginx-build.patch
```

6. Commit the nginx-build.patch into the repository and adjust the .gitlab-ci.yml to [reference](https://github.com/kubernetes/ingress-nginx/tree/master/images/nginx) the versions.

```
  INGRESS_VER: "0.25.0"
  NGINX_VER: "0.90"
```

# CI Workflow Description

The following steps are implemented in the CI and correspond to the upstream build process: use minimum image to build nginx and then build the controller image based on that.

## Step 1: Nginx from base image

```bash
~$ curl -L https://github.com/kubernetes/ingress-nginx/archive/nginx-0.23.0.tar.gz | tar zxv
~$ cd ingress-nginx-0.23.0/images/nginx/rootfs
~$ patch build.sh < ../../../../nginx-build.patch
~$ cd ../ && make sub-container-amd64

~$ docker tag quay.io/kubernetes-ingress-controller/nginx:0.80 gitlab.cern.ch/batch-team/infra/kubernetes-ingress-nginx-controller/nginx:0.80
~$ docker push gitlab.cern.ch/batch-team/infra/kubernetes-ingress-nginx-controller/nginx:0.80
```

# Step 2: Controller build

Based on the image from the previous step we can now build the controller image as follows:

```bash
~$ cd ../../ 
~$ sed -i 's,BASEIMAGE?=\(.*\),BASEIMAGE?=gitlab.cern.ch/batch-team/infra/kubernetes-ingress-nginx-controller/nginx:0.80,' Makefile.backup
~$ make sub-container-amd64

~$ docker tag quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.23.0 gitlab.cern.ch/batch-team/infra/kubernetes-ingress-nginx-controller/nginx-ingress-controller:0.23.0
~$ docker push gitlab.cern.ch/batch-team/infra/kubernetes-ingress-nginx-controller/nginx-ingress-controller:0.23.0
```
